package bank;

import java.util.Collection;
import bank.account.*;

public interface BankProc {

	/**
	 * @param acc
	 * @pre acc != NULL
	 * @post getSize() = getSize()@pre + 1
	 * 
	 * @return if the action was successfully performed
	 */
	public boolean addAccount(Account acc);

	/**
	 * 
	 * @param acc
	 * @pre acc != NULL
	 * @pre containsAccount(acc) == true
	 * @post getSize() = getSize()@pre - 1
	 * @post containsAccount(acc) == false
	 * @return if the action was successfully performed, returned value != NULL
	 */
	public boolean removeAccount(Account acc);

	/**
	 * @pre true
	 * @post @nochange
	 * @return if the bank is well formed, meaning all values are attributed to keys being their owner 
	 * 			and the container having container.size() keys
	 */
	public boolean isWellFormed();

	
	public void readAccounts();
	
	public void writeAccounts();
	
	public Collection<Account> getAccounts();
	
	public Collection<Person> getPersons();
	
	public void removeAccounts(Person person);
}
