package bank;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import bank.account.Account;
import bank.account.Person;
import bank.account.SavingAccount;

import com.google.common.collect.HashMultimap;

/**
 * Bank class
 * @author George
 *
 */
public class Bank implements BankProc {
	private final String SERIALIZATION_FILE = "bankAccounts.dat";
	
	
	private volatile HashMultimap<Person, Account> container;
	private volatile AtomicInteger currentCycle = new AtomicInteger(0);

	private static Bank instance;

	public static Bank getInstance() {
		if (instance == null) {
			instance = new Bank();
		}
		return instance;
	}

	private Bank() {
		container = HashMultimap.<Person, Account> create();
		ScheduledThreadPoolExecutor ex = new ScheduledThreadPoolExecutor(1);
		ex.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				System.out.printf("------- Cycle %d -------\n", currentCycle.get());
				for (Account acc : container.values()) {
					if (acc instanceof SavingAccount) {
						System.out.println(acc);
						((SavingAccount) acc).updateInterestBalance();
					}
				}
				currentCycle.incrementAndGet();
			}
		}, SavingAccount.MILLISECOND_INTEREST_PERIOD, SavingAccount.MILLISECOND_INTEREST_PERIOD, TimeUnit.MILLISECONDS);
	}

	/**
	 * add an account to the bank
	 */
	@Override
	synchronized public boolean addAccount(Account acc) {
		assert acc != null;

		int initContainerSize = container.size();
		boolean success = container.put(acc.getOwner(), acc);

		assert container.size() == initContainerSize + 1;
		assert isWellFormed();

		return success;
	}

	/**
	 * Remove an account from the bank
	 */
	@Override
	synchronized public boolean removeAccount(Account acc) {
		assert acc != null;

		int initContainerSize = container.size();
		boolean success = container.remove(acc.getOwner(), acc);

		assert container.size() == initContainerSize - 1;
		assert isWellFormed();

		return success;
	}

	@Override
	public String toString() {
		return null;
	}

	/**
	 * Verifies that the bank is well formed
	 */
	@Override
	public boolean isWellFormed() {
		boolean correctLinkage = true;
		int foundAccounts = 0;
		boolean foundAccountsFits;
		boolean uniquePerons = true;

		List<Person> persons = new ArrayList<>(container.keySet());
		Person first, second;
		for (int i = 0; i < persons.size() - 1; i++) {
			for (int j = i + 1; j < persons.size(); j++) {
				first = persons.get(i);
				second = persons.get(j);
				if (first.equals(second) || first.getCNP() == second.getCNP()) {
					uniquePerons = false;
				}
			}
		}

		synchronized (container) {
			for (Person key : container.keySet()) {
				for (Account value : container.asMap().get(key)) {
					foundAccounts++;
					if (!value.getOwner().equals(key)) {
						correctLinkage = false;
					}
				}
			}
			foundAccountsFits = foundAccounts == container.size();
		}

		return uniquePerons && correctLinkage && foundAccountsFits;
	}


	/**
	 * Read the accounts from the file "bankAccounts.dat"
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void readAccounts() {
		ObjectInputStream is = null;
		try {
			is = new ObjectInputStream(new FileInputStream(SERIALIZATION_FILE));
			container = (HashMultimap<Person, Account>) is.readObject();
		} catch (IOException | ClassNotFoundException e) {
			// e.printStackTrace();
			System.out.println("Serialization files could not be found/opened/read.");
			container = HashMultimap.<Person, Account> create();
		} finally {
			try {
				is.close();
			} catch (Exception ioe) {
				ioe.printStackTrace();
				System.exit(1);
			}
		}
		assert isWellFormed();
	}

	/**
	 * Write the accounts to the file "bankAccounts.dat"
	 */
	@Override
	public void writeAccounts() {
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(new FileOutputStream(SERIALIZATION_FILE));
			os.writeObject(container);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Serializtion file could not be written.");
		} finally {
			try {
				os.close();
			} catch (Exception ioe) {
				ioe.printStackTrace();
				System.exit(1);
			}
		}
		assert isWellFormed();
	}

	/**
	 * Return all the accounts
	 */
	@Override
	public Collection<Account> getAccounts() {
		assert isWellFormed();
		return Collections.unmodifiableCollection(container.values());
	}

	/**
	 * Return all the persons
	 */
	@Override
	public Collection<Person> getPersons() {
		return Collections.unmodifiableCollection(container.keySet());
	}

	/**
	 * Remove all the accounts from a certain person
	 */
	@Override
	public void removeAccounts(Person person) {
		container.removeAll(person);
	}
}
