package bank;

import bank.account.Person;
import bank.account.SavingAccount;
import bank.account.SpendingAccount;
import bank.gui.Gui;

public class BankManagement {
	public static void main(String[] args) {
		final Bank bank = Bank.getInstance();
		
		// bank.readAccounts();

		final Person andrew = new Person(11, "Andrew");
		final Person john = new Person(12, "John");
		final Person marcus = new Person(13, "Marcus");		
		bank.addAccount(new SavingAccount(andrew, 100));
		bank.addAccount(new SavingAccount(john, 200));
		bank.addAccount(new SavingAccount(marcus, 350));
		bank.addAccount(new SpendingAccount(marcus, 0));
//		
		Gui gui = new Gui();
		gui.setVisible(true);
		

	}
}
