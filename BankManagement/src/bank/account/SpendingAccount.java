package bank.account;
import org.junit.Assert;

/**
 * Spending Account
 * @author George
 *
 */
public class SpendingAccount extends Account {
	private static final long serialVersionUID = 1L;
	public final double MIN_EXTRACTABLE_SUM = 10.0;
	public final double MIN_ABSOLUTE_COMISSION_FEE = 5.0;
	public final double RELATIVE_COMISSION_FEE = 0.05;

	public SpendingAccount(Person owner) {
		super(owner);
	}

	public SpendingAccount(Person owner, double initBalance) {
		super(owner, initBalance);
	}

	/**
	 * @pre sum > MIN_EXTRACTABLE_SUM
	 * @pre initialBalance >= sum + comissionFee
	 * @post getBalance() = getBalance()@pre - sum - comissionFee
	 */
	@Override
	public synchronized void extractBalance(double sum) {
		double initialBalance = getBalance();
		double comissionFee = Math.max(MIN_ABSOLUTE_COMISSION_FEE, RELATIVE_COMISSION_FEE * sum);

		assert sum > MIN_EXTRACTABLE_SUM;
		assert initialBalance >= sum + comissionFee;

		super.extractBalance(sum + comissionFee);
		Assert.assertEquals(initialBalance - sum - comissionFee, getBalance(), 0.001);
	}
	
	@Override
	public String toString() {
		return "Sp" + super.toString();
	}
}
