package bank.account;
import java.io.Serializable;

/**
 * Person class
 * @author George
 *
 */
public class Person implements Serializable {
	private static final long serialVersionUID = 1L;
	private final String NAME;
	private final int CNP;

	public Person(int id, String name) {
		NAME = name;
		CNP = id;
	}

	public String getName() {
		return NAME;
	}

	public int getCNP() {
		return CNP;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Person)) {
			return false;
		}
		Person that = (Person) obj;
		return (this.CNP == that.CNP) && (this.NAME == that.NAME);
	}

	@Override
	public String toString() {
		return NAME + " (" + CNP + ")";
	}
}
