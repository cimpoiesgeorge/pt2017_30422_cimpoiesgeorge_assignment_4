package bank.account;
/**
 * Saving account
 * @author George
 *
 */
public class SavingAccount extends Account {
	private static final long serialVersionUID = 1L;
	public static final int MILLISECOND_INTEREST_PERIOD = 10000;
	public static final double INTEREST_RATE = 0.075;

	public SavingAccount(final Person owner) {
		this(owner, 0);
	}

	public SavingAccount(final Person owner, double balance) {
		super(owner, balance);
	}

	public synchronized void updateInterestBalance() {
		suplementBalance(SavingAccount.INTEREST_RATE * getBalance());
	}

	@Override
	public String toString() {
		return "Sv" + super.toString();
	}
}
