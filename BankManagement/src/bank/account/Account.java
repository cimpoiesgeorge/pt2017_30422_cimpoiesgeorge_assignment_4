package bank.account;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 * @author George
 *
 */
public abstract class Account implements Serializable {
	private static final long serialVersionUID = 1L;

	protected final Person owner;
	protected double balance;
	protected final int accId;

	public static AtomicInteger incrementer = new AtomicInteger();

	public Account(Person owner) {
		this(owner, 0);
	}

	public Account(Person owner, double initBalance) {
		this.accId = incrementer.getAndIncrement();
		this.owner = owner;
		this.balance = initBalance;

	}

	/**
	 * 
	 * @param sum
	 * @pre sum > 0
	 * @post balance = balance@pre + sum
	 */
	public synchronized void suplementBalance(double sum) {
		assert sum >= 0;
		double prevBalance = this.balance;
		this.balance += sum;
		assert balance == prevBalance + sum;
	}

	/**
	 * 
	 * @param sum
	 * @pre sum > 0 && balance >= sum
	 * @post balance = balance@pre - sum
	 */
	public synchronized void extractBalance(double sum) {
		assert (sum > 0) && (balance >= sum);
		double prevBalance = this.balance;
		this.balance -= sum;
		assert balance == prevBalance - sum;
	}

	
	public Person getOwner() {
		return owner;
	}

	public synchronized double getBalance() {
		return balance;
	}

	@Override
	public String toString() {
		return String.format("[%s, %.2f$]", getOwner(), getBalance());
	}

	public int getId() {
		return accId;
	}
}
