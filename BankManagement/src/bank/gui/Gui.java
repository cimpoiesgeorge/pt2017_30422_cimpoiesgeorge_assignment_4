package bank.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import bank.Bank;
import bank.account.Account;
import bank.account.Person;
import bank.account.SavingAccount;
import bank.account.SpendingAccount;
import javax.swing.JComboBox;

public class Gui {

	private Bank bank = Bank.getInstance();

	private JFrame frame;
	private JTable accountTable;
	private DefaultTableModel accountTableModel = new DefaultTableModel(new Object[][] {}, new Object[] { "PersonId", "Person", "Account Id",
			"AccType", "Balance" });
	private JTextField txtBalance;
	private JList<Person> personList;
	private DefaultListModel<Person> personListModel;
	private JTextField txtInsertPersonName;
	private JTextField txtInsertPersonCNP;
	private JLabel lblCnp;
	private JLabel lblName;
	private JButton btnDeletePerson;

	public static int getScreenWidth() {
		return java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().width;
	}

	public static int getScreenHeight() {
		return java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height;
	}

	private int frameWidth = 750;
	private int frameHeight = 570;
	private JTextField txtInitBalance;
	private JTabbedPane balanceTabbedPane;
	private JButton btnCreateAccount;

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Bank Management");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setResizable(false);

		frame.setBounds(getScreenWidth() / 2 - frameWidth / 2, getScreenHeight() / 2 - frameHeight / 2, frameWidth, frameHeight);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 727, 234);
		frame.getContentPane().add(scrollPane);

		accountTable = new JTable(accountTableModel) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		accountTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		scrollPane.setViewportView(accountTable);

		final JButton btnAddSum = new JButton("Add Sum");
		final JButton btnExtractSum = new JButton("Extract Sum");

		ActionListener supplementExtractListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (accountTable.getSelectedRow() != -1 && !txtBalance.getText().isEmpty()) {
					int selectedAccId = -1;
					double deltaBalance = 0;
					try {
						selectedAccId = Integer.parseInt(accountTableModel.getValueAt(accountTable.getSelectedRow(), 2).toString());
						deltaBalance = Double.parseDouble(txtBalance.getText());
						if (deltaBalance < 0) {
							System.err.println("Balance has to be positive!");
							return;
						}

						for (Account acc : bank.getAccounts()) {
							if (acc.getId() == selectedAccId) {
								if (e.getSource() == btnAddSum) {
									acc.suplementBalance(deltaBalance);
									System.out.printf("Account:%s was suplemented with %f$\n", acc, deltaBalance);
								} else if (e.getSource() == btnExtractSum) {
									acc.extractBalance(deltaBalance);
									System.out.printf("From account:%s, %f$ were extracted\n", acc, deltaBalance);
								}
							}
						}
						refreshAccountsTable();
					} catch (NumberFormatException nfe) {
						nfe.printStackTrace();
					}
				}
			}
		};

		btnExtractSum.addActionListener(supplementExtractListener);
		btnAddSum.addActionListener(supplementExtractListener);
		btnAddSum.setBounds(108, 256, 97, 23);
		frame.getContentPane().add(btnAddSum);

		txtBalance = new JTextField();
		txtBalance.setBounds(10, 256, 86, 20);
		frame.getContentPane().add(txtBalance);
		txtBalance.setColumns(10);

		btnExtractSum.setBounds(215, 256, 110, 23);
		frame.getContentPane().add(btnExtractSum);

		personListModel = new DefaultListModel<>();

		JTabbedPane personsTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		personsTabbedPane.setBounds(10, 290, 195, 183);

		frame.getContentPane().add(personsTabbedPane);
		personList = new JList<>(personListModel);
		personsTabbedPane.addTab("Persons", null, personList, null);
		personList.setLayoutOrientation(JList.VERTICAL_WRAP);
		personList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		txtInsertPersonName = new JTextField();
		txtInsertPersonName.setBounds(53, 484, 74, 20);
		frame.getContentPane().add(txtInsertPersonName);
		txtInsertPersonName.setColumns(10);

		JButton btnInsertPerson = new JButton("Insert");
		btnInsertPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int cnp = Integer.parseInt(txtInsertPersonCNP.getText());
					String name = txtInsertPersonName.getText();
					Person newPerson = new Person(cnp, name);
					if (!personListModel.contains(newPerson)) {
						personListModel.addElement(newPerson);
					}
				} catch (NumberFormatException nfe) {
					nfe.printStackTrace();
				}
			}
		});
		btnInsertPerson.setBounds(131, 508, 74, 23);
		frame.getContentPane().add(btnInsertPerson);

		txtInsertPersonCNP = new JTextField();
		txtInsertPersonCNP.setColumns(10);
		txtInsertPersonCNP.setBounds(53, 509, 74, 20);
		frame.getContentPane().add(txtInsertPersonCNP);

		lblCnp = new JLabel("CNP:");
		lblCnp.setBounds(10, 512, 46, 14);
		frame.getContentPane().add(lblCnp);

		lblName = new JLabel("Name:");
		lblName.setBounds(10, 487, 46, 14);
		frame.getContentPane().add(lblName);

		btnDeletePerson = new JButton("Delete");
		btnDeletePerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person selectedPerson = personList.getSelectedValue();
				if (selectedPerson != null) {
					bank.removeAccounts(selectedPerson);
					personListModel.removeElement(selectedPerson);
				}
			}
		});
		btnDeletePerson.setBounds(131, 483, 74, 23);
		frame.getContentPane().add(btnDeletePerson);

		JTabbedPane accountTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		accountTabbedPane.setBounds(354, 346, 148, 48);

		frame.getContentPane().add(accountTabbedPane);

		final JComboBox<String> accType = new JComboBox<String>();
		accountTabbedPane.addTab("Account Type", accType);

		accType.addItem(new String("Saving Account"));
		accType.addItem(new String("Spending Account"));

		JButton btnDeleteAcc = new JButton("Delete");
		btnDeleteAcc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int selectedRow = accountTable.getSelectedRow();
				if (selectedRow != -1) {
					int selectedAccId = Integer.parseInt(accountTableModel.getValueAt(accountTable.getSelectedRow(), 2).toString());

					Account toBeRemoved = null;
					for (Account acc : bank.getAccounts()) {
						if (acc.getId() == selectedAccId) {
							toBeRemoved = acc;
						}
					}
					if (toBeRemoved != null)
						bank.removeAccount(toBeRemoved);
				}
			}
		});
		btnDeleteAcc.setBounds(335, 256, 89, 23);
		frame.getContentPane().add(btnDeleteAcc);

		balanceTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		balanceTabbedPane.setBounds(512, 346, 123, 48);
		frame.getContentPane().add(balanceTabbedPane);

		txtInitBalance = new JTextField();
		balanceTabbedPane.addTab("Initial Balance", txtInitBalance);
		txtInitBalance.setColumns(10);

		btnCreateAccount = new JButton("Create Account");
		btnCreateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (personList.getSelectedValue() != null) {
					if (!txtInitBalance.getText().isEmpty()) {
						if (accType.getSelectedIndex() == 0) {
							bank.addAccount(new SavingAccount(personList.getSelectedValue(), Double.parseDouble(txtInitBalance.getText())));
						}

						if (accType.getSelectedIndex() == 1) {
							bank.addAccount(new SpendingAccount(personList.getSelectedValue(), Double.parseDouble(txtInitBalance.getText())));
						}
					}
				}
			}
		});
		btnCreateAccount.setBounds(428, 429, 123, 23);
		frame.getContentPane().add(btnCreateAccount);

		JLabel lblSelectAPerson = new JLabel("Select a person from the list on the left side of the window");
		lblSelectAPerson.setBounds(322, 405, 343, 14);
		frame.getContentPane().add(lblSelectAPerson);
		
		JButton btnLoad = new JButton("Load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				personList.removeAll();
				bank.readAccounts();
			}
		});
		btnLoad.setBounds(648, 256, 89, 23);
		frame.getContentPane().add(btnLoad);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bank.writeAccounts();
			}
		});
		btnSave.setBounds(549, 256, 89, 23);
		frame.getContentPane().add(btnSave);

		new ScheduledThreadPoolExecutor(1).scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				refreshAccountsTable();
				refreshPersons();
			}
		}, 0, 300, TimeUnit.MILLISECONDS);
	}

	private void refreshAccountsTable() {
		int currSelectedRow = accountTable.getSelectedRow();

		accountTableModel.setRowCount(0);
		for (Account acc : bank.getAccounts()) {
			String type = (acc instanceof SavingAccount) ? "Saving" : "Spending";
			accountTableModel.addRow(new Object[] { acc.getOwner().getCNP(), acc.getOwner().getName(), acc.getId(), type,
					String.format("%.2f$", acc.getBalance()) });
		}

		currSelectedRow = Math.min(currSelectedRow, accountTable.getRowCount() - 1);
		accountTable.getSelectionModel().setSelectionInterval(currSelectedRow, currSelectedRow);

	}

	private void refreshPersons() {
		for (Person p : bank.getPersons()) {
			if (!personListModel.contains(p)) {
				personListModel.addElement(p);
			}
		}

	}

	public void setVisible(boolean visible) {
		frame.setVisible(visible);
	}
}
